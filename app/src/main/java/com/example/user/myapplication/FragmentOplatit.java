package com.example.user.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;


public class FragmentOplatit extends Fragment {
    RadioButton oplgod, oplmes, oplcard, oplGoogle;
     Button button;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.activity_fragment_oplatit,container,false);
        oplgod=view.findViewById(R.id.opl_na_god);
        oplmes=view.findViewById(R.id.opl_na_3mes);
        oplcard=view.findViewById(R.id.card);
        oplGoogle=view.findViewById(R.id.gplay);
        button=view.findViewById(R.id.oplatit);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
          if(oplgod.isChecked() && (oplcard.isChecked())){
              Intent intent=new Intent(getActivity().getBaseContext(), MyCard.class);
              intent.putExtra("key", "1000");
              getActivity().startActivity(intent);
              getActivity().fileList();
          }else{
              if(oplmes.isChecked() && (oplcard.isChecked())){
                  Intent intent=new Intent(getActivity().getBaseContext(), MyCard.class);
                  intent.putExtra("key", "400");
                  getActivity().startActivity(intent);
                  getActivity().fileList();
          } else {
                  if (oplgod.isChecked() && (oplGoogle.isChecked())) {
                      Intent intent = new Intent(getActivity().getBaseContext(), GooglePlay.class);
                      intent.putExtra("key", "1000");
                      getActivity().startActivity(intent);
                      getActivity().fileList();
                  } else {
                      if (oplmes.isChecked() && (oplGoogle.isChecked())) {
                          Intent intent = new Intent(getActivity().getBaseContext(), GooglePlay.class);
                          intent.putExtra("key", "400");
                          getActivity().startActivity(intent);
                          getActivity().fileList();
                      }else{
                          Toast.makeText(getContext(), "error", Toast.LENGTH_SHORT).show();
                      }
                  }
              }
          }
            }
        });

     return  view;
    }
}
