package com.example.user.myapplication;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button=findViewById(R.id.oplatit_podpisku);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //buttoni clicki depqum linearlayoutin talis enq merfragment@
                FragmentOplatit fragmentOplatit=new FragmentOplatit();
                FragmentManager fragmentManager=getSupportFragmentManager();
                fragmentManager.beginTransaction().add(R.id.linearlayout, fragmentOplatit).commit();
            }
        });


    }
}
