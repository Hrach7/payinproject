package com.example.user.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.cloudipsp.android.Card;
import com.cloudipsp.android.CardInputLayout;
import com.cloudipsp.android.Cloudipsp;
import com.cloudipsp.android.CloudipspWebView;
import com.cloudipsp.android.Currency;
import com.cloudipsp.android.Order;
import com.cloudipsp.android.Receipt;

public class MyCard extends AppCompatActivity {


    private static final int MERCHANT_ID = 1396424;
 //haytararum em view elemnters
    private EditText editAmount;
    private Spinner spinnerCcy;
    private EditText editEmail;
    private EditText editDescription;
    private CardInputLayout cardLayout;
    private CloudipspWebView webView;
    private Button button;
    private Cloudipsp cloudipsp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_card);
 //inicializacnum em viewer@;
        editAmount =  findViewById(R.id.edit_amount);
        spinnerCcy = findViewById(R.id.spinner_ccy);
        editEmail =  findViewById(R.id.edit_email);
        editDescription = findViewById(R.id.edit_description);
        cardLayout =  findViewById(R.id.card_layout);
        button=findViewById(R.id.btn_pay);
//CloudipspWebviewi webview object@ 3Dsecurityov stugum a anvtangutyun@
        webView = findViewById(R.id.web_view);

        //Cloudispsi cloudipsp objectov katarum a vcharum@
        cloudipsp = new Cloudipsp(MERCHANT_ID, webView);
        // spinnerCCy-in adaptri mijocov talis enq Currency_i value-ner@, vcharman poxarjeqnern en
        spinnerCcy.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, Currency.values()));

      //yete usern @ntrel a podpiska mi tarov gumari tex@ grvum a 1000, yete 3 amsov 400
        //es tox@ karam grem valyutan nsheluc heto, u kaxvac inch valyut a @ntrel kursov hashvi gri
        //orinak rubli a nshel-1000, dollar 1000/65,
        //isk aveli chisht valyutayi voreve API-ic ogtvel u online hashvel
        editAmount.setText(getIntent().getStringExtra("key"));

        //kanchum em vcharman metod@ oplatit buttoni clickov
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processPay();
            }
        });
    }
    //metod tvyalneri pahpanman hamar
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }


    private void processPay() {
        // stugum em dashteri lracman chtutyun@, yete sxal ka setError metodov cuyc em talis yete amen inch chisht a
        //Cardi card objectin em talis useri cardi tvyalner@
        editAmount.setError(null);
        editEmail.setError(null);
        editDescription.setError(null);

        final int amount;
        try {
            amount = Integer.valueOf(editAmount.getText().toString());
        } catch (Exception e) {
            editAmount.setError("Invalid amount");
            return;
        }

        final String email = editEmail.getText().toString();
        final String description = editDescription.getText().toString();
        if (TextUtils.isEmpty(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            editEmail.setError("Invalid email");
        } else if (TextUtils.isEmpty(description)) {
            editDescription.setError("Invalid description");
        } else {
            final Card card = cardLayout.confirm(new CardInputLayout.ConfirmationErrorHandler() {
                @Override
                public void onCardInputErrorClear(CardInputLayout view, EditText editText) {
                }

                @Override
                public void onCardInputErrorCatched(CardInputLayout view, EditText editText, String error) {
                    editText.setError(error);
                }
            });
               //stugum em yete card@ stexcav a, orderin em talis useri tvyalner@, gumari chap@, valyutan
            // katarman jam@
            if (card != null) {
                final Currency currency = (Currency) spinnerCcy.getSelectedItem();
                final Order order = new Order(amount, currency, "vb_" + System.currentTimeMillis(), description, email);
                order.setLang(Order.Lang.ru);
                //katarum em vcharum@
                cloudipsp.pay(card, order, new Cloudipsp.PayCallback() {
                    @Override
                    public void onPaidProcessed(Receipt receipt) {
                        Toast.makeText(MyCard.this, "Paid " + receipt.status.name() + "\nPaymentId:" + receipt.paymentId, Toast.LENGTH_LONG).show();
                    }
                    //stugum em execptionner@ yete ka cuyc a talis Toastov
                    @Override
                    public void onPaidFailure(Cloudipsp.Exception e) {
                        if (e instanceof Cloudipsp.Exception.Failure) {
                            Cloudipsp.Exception.Failure f = (Cloudipsp.Exception.Failure) e;

                            Toast.makeText(MyCard.this, "Failure\nErrorCode: " +
                                    f.errorCode + "\nMessage: " + f.getMessage() + "\nRequestId: " + f.requestId, Toast.LENGTH_LONG).show();
                        } else if (e instanceof Cloudipsp.Exception.NetworkSecurity) {
                            Toast.makeText(MyCard.this, "Network security error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        } else if (e instanceof Cloudipsp.Exception.ServerInternalError) {
                            Toast.makeText(MyCard.this, "Internal server error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        } else if (e instanceof Cloudipsp.Exception.NetworkAccess) {
                            Toast.makeText(MyCard.this, "Network error", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(MyCard.this, "Payment Failed", Toast.LENGTH_LONG).show();
                        }
                        e.printStackTrace();
                    }
                });
            }
        }
    }

    // mer webviewum 3D Securyi miijocov tanum a ira bank cackagir@ grelu hamar
    @Override
    public void onBackPressed() {
        if (webView.waitingForConfirm()) {
            webView.skipConfirm();
        } else {
            super.onBackPressed();
        }
    }
}
